/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef CPP_PLI_DataExchangeProxy_H
#define CPP_PLI_DataExchangeProxy_H

#include <string>

#include <Ice/Ice.h>
#include <IceGrid/IceGrid.h>

#include <MSeg/interface/DataExchangeInterface.h>

namespace plicpp
{
    class DataExchangeProxy
    {

        private:
            mseg::DataExchangeInterfacePrx data;

        public:
            DataExchangeProxy();
            virtual ~DataExchangeProxy();
            void connect(Ice::CommunicatorPtr ic);

            int getFrameCount();
            std::string getMMMFile();
            std::string getViconFile();
            std::vector<int> getGroundTruth();
            std::vector<float> getJointAnglesForFrame(int n);

            void reportKeyFrame(int frameNumber);
            void reportKeyFrames(std::vector<int>);

    };
}

#endif
