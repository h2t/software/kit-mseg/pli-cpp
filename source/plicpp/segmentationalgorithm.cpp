/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "segmentationalgorithm.h"

plicpp::SegmentationAlgorithm::SegmentationAlgorithm()
{
    this->parameters["parameters"] = Json::arrayValue;

    this->data = new plicpp::DataExchangeProxy();
}

plicpp::SegmentationAlgorithm::~SegmentationAlgorithm()
{
    delete this->data;
}

plicpp::DataExchangeProxy*
plicpp::SegmentationAlgorithm::getData()
{
    return this->data;
}

void
plicpp::SegmentationAlgorithm::train()
{

}

void
plicpp::SegmentationAlgorithm::resetTraining()
{

}

bool
plicpp::SegmentationAlgorithm::internal_requiresTraining(const ::Ice::Current&)
{
    return this->requiresTraining;
}

void
plicpp::SegmentationAlgorithm::internal_resetTraining(const Ice::Current&)
{
    this->resetTraining();
}

mseg::Granularity
plicpp::SegmentationAlgorithm::internal_getTrainingGranularity(const Ice::Current&)
{
    return this->trainingGranularity;
}

void
plicpp::SegmentationAlgorithm::internal_train(const ::Ice::Current&)
{
    this->train();
}

void
plicpp::SegmentationAlgorithm::internal_segment(const Ice::Current&)
{
    this->segment();
}

std::string
plicpp::SegmentationAlgorithm::internal_getName(const Ice::Current&)
{
    return this->name;
}

std::string
plicpp::SegmentationAlgorithm::internal_getParameters(const Ice::Current&)
{
    Json::FastWriter writer;

    return writer.write(this->parameters);
}

void
plicpp::SegmentationAlgorithm::internal_setParameters(const std::string& parametersJson, const Ice::Current&)
{
    Json::Reader reader;
    Json::Value parameterValues;

    reader.parse(parametersJson, parameterValues);

    int paramCount = (int) parameterValues["parameters"].size();

    for (int i = 0; i < (int) paramCount; i++)
    {
        std::string name = parameterValues["parameters"][i]["name"].asString();
        std::string value = parameterValues["parameters"][i]["value"].asString();

        this->setParameterValue(name, value);
    }
}

bool
plicpp::SegmentationAlgorithm::isParameterRegistered(std::string name)
{
    return (this->findParameterByName(name) >= 0);
}

void
plicpp::SegmentationAlgorithm::setParameterValue(std::string name, std::string value)
{
    int index;

    if ((index = this->findParameterByName(name)) < 0)
    {
        throw "Unknown parameter '" + name + "'";
    }

    this->parameters["parameters"][index]["value"] = value;
}

int
plicpp::SegmentationAlgorithm::findParameterByName(std::string name)
{
    int paramCount = (int) this->parameters["parameters"].size();

    for (int i = 0; i < (int) paramCount; i++)
    {
        if (this->parameters["parameters"][i]["name"].asString() == name)
        {
            return i;
        }
    }

    return -1;
}

void
plicpp::SegmentationAlgorithm::registerIntParameter(std::string name, std::string description = "", int defaultValue = 0, int min = std::numeric_limits<int>::min(), int max = std::numeric_limits<int>::max())
{
    if (this->isParameterRegistered(name))
    {
        throw "Parameter with the name '" + name + "' already registered";
    }

    Json::Value parameter;

    parameter["name"] = name;
    parameter["description"] = description;
    parameter["type"] = "int";
    parameter["defaultValue"] = std::to_string(defaultValue);
    parameter["intmin"] = min;
    parameter["intmax"] = max;

    this->parameters["parameters"].append(parameter);
}

void
plicpp::SegmentationAlgorithm::registerFloatParameter(std::string name, std::string description = "", double defaultValue = 0.0, int decimals = 2, double min = -std::numeric_limits<double>::max(), double max = std::numeric_limits<double>::max())
{
    if (this->isParameterRegistered(name))
    {
        throw "Parameter with the name '" + name + "' already registered";
    }

    Json::Value parameter;

    parameter["name"] = name;
    parameter["description"] = description;
    parameter["type"] = "float";
    parameter["defaultValue"] = std::to_string(defaultValue);
    parameter["floatmin"] = min;
    parameter["floatmax"] = max;
    parameter["decimals"] = decimals;

    this->parameters["parameters"].append(parameter);
}

void
plicpp::SegmentationAlgorithm::registerBoolParameter(std::string name, std::string description = "", bool defaultValue = false)
{
    if (this->isParameterRegistered(name))
    {
        throw "Parameter with the name '" + name + "' already registered";
    }

    Json::Value parameter;

    parameter["name"] = name;
    parameter["description"] = description;
    parameter["type"] = "bool";
    parameter["defaultValue"] = defaultValue ? "true" : "false";

    this->parameters["parameters"].append(parameter);
}

void
plicpp::SegmentationAlgorithm::registerStringParameter(std::string name, std::string description = "", std::string defaultValue = "")
{
    if (this->isParameterRegistered(name))
    {
        throw "Parameter with the name '" + name + "' already registered";
    }

    Json::Value parameter;

    parameter["name"] = name;
    parameter["description"] = description;
    parameter["type"] = "string";
    parameter["defaultValue"] = defaultValue;

    this->parameters["parameters"].append(parameter);
}

void
plicpp::SegmentationAlgorithm::registerJsonParameter(std::string name, std::string description = "", Json::Value defaultValue = "")
{
    if (this->isParameterRegistered(name))
    {
        throw "Parameter with the name '" + name + "' already registered";
    }

    Json::FastWriter writer;
    Json::Value parameter;

    parameter["name"] = name;
    parameter["description"] = description;
    parameter["type"] = "string";
    parameter["defaultValue"] = writer.write(defaultValue);

    this->parameters["parameters"].append(parameter);
}

int
plicpp::SegmentationAlgorithm::getIntParameter(std::string name)
{
    int index;

    if ((index = this->findParameterByName(name)) < 0)
    {
        throw "Unknown parameter '" + name + "'";
    }

    return std::stoi(this->parameters["parameters"][index]["value"].asString());
}

double
plicpp::SegmentationAlgorithm::getFloatParameter(std::string name)
{
    int index;

    if ((index = this->findParameterByName(name)) < 0)
    {
        throw "Unknown parameter '" + name + "'";
    }

    return std::stod(this->parameters["parameters"][index]["value"].asString());
}

bool
plicpp::SegmentationAlgorithm::getBoolParameter(std::string name)
{
    int index;

    if ((index = this->findParameterByName(name)) < 0)
    {
        throw "Unknown parameter '" + name + "'";
    }

    return (boost::to_lower_copy(this->parameters["parameters"][index]["value"].asString()) == "true");
}

std::string
plicpp::SegmentationAlgorithm::getStringParameter(std::string name)
{
    int index;

    if ((index = this->findParameterByName(name)) < 0)
    {
        throw "Unknown parameter '" + name + "'";
    }

    return this->parameters["parameters"][index]["value"].asString();
}

Json::Value
plicpp::SegmentationAlgorithm::getJsonParameter(std::string name)
{
    int index;

    if ((index = this->findParameterByName(name)) < 0)
    {
        throw "Unknown parameter '" + name + "'";
    }

    Json::Value value;
    Json::Reader reader;

    reader.parse(this->parameters["parameters"][index]["value"].asString(), value);

    return value;
}
