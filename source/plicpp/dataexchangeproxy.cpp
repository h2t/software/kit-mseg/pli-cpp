/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "dataexchangeproxy.h"

plicpp::DataExchangeProxy::DataExchangeProxy()
{
    
}

plicpp::DataExchangeProxy::~DataExchangeProxy()
{

}

void
plicpp::DataExchangeProxy::connect(Ice::CommunicatorPtr ic)
{
    Ice::ObjectPrx de = ic->stringToProxy("DataExchange");
    this->data = mseg::DataExchangeInterfacePrx::checkedCast(de);
}

int
plicpp::DataExchangeProxy::getFrameCount()
{
    return this->data->getFrameCount();
}

std::string
plicpp::DataExchangeProxy::getMMMFile()
{
    return this->data->getMMMFile();
}

std::string
plicpp::DataExchangeProxy::getViconFile()
{
    return this->data->getViconFile();
}

std::vector<int>
plicpp::DataExchangeProxy::getGroundTruth()
{
    return this->data->getGroundTruth();
}

std::vector<float>
plicpp::DataExchangeProxy::getJointAnglesForFrame(int n)
{
    return this->data->getJointAnglesForFrame(n);
}

void
plicpp::DataExchangeProxy::reportKeyFrame(int keyFrame)
{
    this->data->reportKeyFrame(keyFrame);
}

void
plicpp::DataExchangeProxy::reportKeyFrames(std::vector<int> keyFrames)
{
    this->data->reportKeyFrames(keyFrames);
}
