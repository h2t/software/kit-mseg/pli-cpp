/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "algorithmserver.h"

Ice::CommunicatorPtr ic;

void shutdownIceConnection(int)
{
    std::cout << "" << std::endl;

    if (ic)
    {
        try
        {
            ic->shutdown();
        }
        catch (const std::exception& e) {} // Ignore exceptions
    }
}

plicpp::AlgorithmServer::AlgorithmServer()
{
    // Initialize SIGINT handler (ctrl+c interrupt)
    {
        struct sigaction sigIntHandler;

        sigIntHandler.sa_handler = shutdownIceConnection;
        sigemptyset(&sigIntHandler.sa_mask);
        sigIntHandler.sa_flags = 0;

        sigaction(SIGINT, &sigIntHandler, NULL);
    }
}

plicpp::AlgorithmServer::~AlgorithmServer()
{
    
}

void
plicpp::AlgorithmServer::serve(plicpp::SegmentationAlgorithm* algorithm)
{
    std::string home(std::getenv("HOME"));
    Ice::StringSeq parameters = {"--Ice.Config=" + home + "/.armarx/default.cfg"};

    ic = Ice::initialize(parameters);

    // Add this algorithm as object to IceGrid
    Ice::ObjectAdapterPtr adapter = ic->createObjectAdapterWithEndpoints(algorithm->internal_getName(), "tcp");
    adapter->activate();

    Ice::Identity id = ic->stringToIdentity(algorithm->internal_getName());
    adapter->add(algorithm, id);

    Ice::ObjectPrx proxy = adapter->createProxy(id);

    // Try to connect to ArmarX' IceGrid
    try
    {
        Ice::ObjectPrx regObj = ic->stringToProxy("IceGrid/Registry");
        IceGrid::RegistryPrx reg = IceGrid::RegistryPrx::checkedCast(regObj);

        IceGrid::AdminSessionPrx adminSession = reg->createAdminSession("user", "password");
        IceGrid::AdminPrx admin = adminSession->getAdmin();

        // Try to add algorithm to ArmarX' IceGrid
        try
        {
            admin->addObjectWithType(proxy, proxy->ice_id());
        }
        // Retry to add algorithm if it already exists
        catch (const IceGrid::ObjectExistsException&)
        {
            // If adding the object fails because of the exception above, there is a stray object from a previous instance...
            std::cout << "Algorithm with the same name already registered. Perhaps a previous instance crashed. Cleaning up now..." << std::endl;

            // ...therefore the stray object will be removed...
            admin->removeObject(id);

            std::cout << "Retrying..." << std::endl;

            // ...and the addition of the object to ArmarX' IceGrid will be retried
            admin->addObjectWithType(proxy, proxy->ice_id());
        }

        // Close session
        adminSession->destroy();

        // Try to connect to DataExchange
        try
        {
            std::cout << "Connecting to MSeg core module..." << std::endl;

            algorithm->getData()->connect(ic);

            std::cout << "Connection established" << std::endl;

            // Indicate that initialization finished successfully and waiting for instructions
            std::cout << algorithm->internal_getName() << " ready" << std::endl;

            // Wait for SIGINT or shutdown
            ic->waitForShutdown();
        }
        // Indicate that user has to start mseg if not registered
        catch (const Ice::NotRegisteredException&)
        {
            std::cout << "MSeg core module not available. Start it by running 'msegcm start'" << std::endl;
        }

        std::cout << "Cleaning up..." << std::endl;

        // Create a new admin session to remove the algorithm from IceGrid and destroy session
        adminSession = reg->createAdminSession("user", "password");
        admin = adminSession->getAdmin();

        admin->removeObject(id);

        adminSession->destroy();
    }
    // Indicate that user has to start armarx
    catch (const Ice::ConnectionRefusedException&)
    {
        std::cout << "ArmarX is not available. Start it by running 'armarx start'" << std::endl;
    }

    if (ic)
    {
        ic->destroy();
    }
}
